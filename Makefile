IMAGE := registry.gitlab.com/abartnik/scan_classifier
DOCKER_VERSION := 0.9.6
TAG := $(IMAGE):$(DOCKER_VERSION)

all: build deploy update

build:
	docker build . -t $(TAG)

deploy: build
	docker push $(TAG)

update: deploy
	docker build . -t $(IMAGE):latest
	docker push $(IMAGE):latest