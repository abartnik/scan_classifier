from enum import Enum


class ScanType(Enum):
    T2star = 0
    DWI = 1
    FLAIR = 2
    fmap = 3
    rsfmri = 4
    FSE = 5
    MPRAGE = 6
    PD = 7
    SPGR = 8
