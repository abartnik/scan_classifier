import os, json
import pydicom
from pydicom.dataset import FileDataset
from pydicom.errors import InvalidDicomError
import pandas as pd
import numpy as np
from typing import Dict
from xgboost import XGBClassifier, DMatrix

from .ScanType import ScanType


model = XGBClassifier()
model.load_model('model.bin')

def impute_missing_header_info(metadata) -> Dict:

    if 'InversionTime' not in metadata.keys():
        metadata['InversionTime'] = 0.0
    if 'RepetitionTime' not in metadata.keys():
        metadata['RepetitionTime'] = 0.0
    if 'EchoTime' not in metadata.keys():
        metadata['EchoTime'] = 0.0
    if 'DiffusionBValue' not in metadata.keys():
        metadata['DiffusionBValue'] = 0.0
    if 'EchoTrainLength' not in metadata.keys():
        metadata['EchoTrainLength'] = 0.0
    if 'FlipAngle' not in metadata.keys():
        metadata['FlipAngle'] = 0.0

    if 'ScanningSequence' not in metadata.keys():
        metadata['ScanningSequence'] = "RM"
    
    return(metadata)

def split_scanning_sequences(metadata) -> Dict:
    seq = metadata['ScanningSequence']
    scanning_sequences = {'SE':0, 'IR':0, 'GR':0, 'EP':0}
    for sequence_name in scanning_sequences.keys():
        if sequence_name in seq:
            scanning_sequences[sequence_name] = 1
    return(scanning_sequences)

def read_dcm_header(dcm_path) -> FileDataset:
    if os.path.isdir(dcm_path):
        for _file in os.listdir(dcm_path):
            try:
                header = pydicom.dcmread( os.path.join(dcm_path, _file) )
            except InvalidDicomError:
                continue
        dcm_file = os.path.join(dcm_path, _file)
    else:
        dcm_file = dcm_path

    try:
        header = pydicom.dcmread(dcm_file)
    except InvalidDicomError:
        print(f"{dcm_path} does not point to a valid DICOM")
        return(1)    
    
    metadata = {}
    params = ['EchoTime', 'InversionTime', 'RepetitionTime', 'DiffusionBValue', 
              'EchoTrainLength', 'FlipAngle']
    for param in params:
        if header.get(param) != None:
            metadata[param] = header.get(param)
        else:
            metadata[param] = 0.0
    if header.get('ScanningSequence') != None:
        metadata['ScanningSequence'] = header.get('ScanningSequence')
    else:
        metadata['ScanningSequence'] = 'RM'
    return(metadata)

def read_bids_sidecar(sidecar_file) -> Dict:
    with open(sidecar_file, 'r') as infile:
        bids_metadata = json.load(infile)
    bids_metadata = impute_missing_header_info(bids_metadata)
    bids_metadata['EchoTime'] = bids_metadata['EchoTime'] * 1000
    bids_metadata['RepetitionTime'] = bids_metadata['RepetitionTime'] * 1000
    bids_metadata['InversionTime'] = bids_metadata['InversionTime'] * 1000
    return(bids_metadata)

def format_input_for_model(metadata) -> np.ndarray:
    full_metadata = impute_missing_header_info(metadata)
    scanning_sequences = split_scanning_sequences(full_metadata)
    model_metadata = {
        'EchoTime': float(full_metadata.get('EchoTime')),
        'InversionTime': float(full_metadata.get('InversionTime')),
        'RepetitionTime': float(full_metadata.get('RepetitionTime')),
        'DiffusionBValue': float(full_metadata.get('DiffusionBValue')),
        'SE': scanning_sequences['SE'],
        'IR': scanning_sequences['IR'],
        'GR': scanning_sequences['GR'],
        'EP': scanning_sequences['EP'],
        'EchoTrainLength': float(full_metadata.get('EchoTrainLength')),
        'FlipAngle': float(full_metadata.get('FlipAngle')),
    }
    model_inputs = pd.DataFrame(model_metadata, index=[0])
    model_inputs = model_inputs.to_numpy(dtype='float32')
    return(model_inputs)

def predict_scan_type(model, input_data):
    predicted_scan_type = model.predict(input_data)
    predicted_certainty = model.predict_proba(input_data)
    return(ScanType(predicted_scan_type), predicted_certainty)

def identify_feature_contributions(model, input_data, predicted_scan_type):
    booster = model.get_booster()
    shap_values = booster.predict(DMatrix(input_data), pred_contribs=True)[0,predicted_scan_type]
    features = [
        "EchoTime", "InversionTime", "RepetitionTime", "DiffusionBValue", 
        "SE", "IR", "GR", "EP", "EchoTrainLength", "FlipAngle", 
        "bias_term"
    ]
    feature_contributions = {}
    for index, i in enumerate(shap_values):
        feature_contributions[features[index]] = i
    return(feature_contributions)
