from setuptools import find_packages, setup


setup(
    name='ScanClassifier',
    packages=find_packages(include=['scan_classifier']),
    version='0.9.4',
    description='Python library used to predict scan type of MRI scan from metadata',
    author='abartnik@bnac.net',
    license='GPL',
    install_requires=[
        'xgboost==1.5.0',
        'pandas',
        'numpy==1.20.3',
        'pydicom==2.0.0',
        'scikit-learn',
        'nipype',
        'begins'
    ]
)
