#!/usr/bin/env python3

# import begin
import fire


# @begin.start
def main(input_scan: str) -> None:
    """
    Predict the MRI acquisition type for a given scan
    Parameters
    ----------
    input_scan : str
        Path to the scan you want to classify
    """
    from scan_classifier_logic import scan_classifier as sc # This includes the model

    if 'json' in input_scan.split('.')[-1]:
        metadata = sc.read_bids_sidecar(input_scan)
    else:
        metadata = sc.read_dcm_header(input_scan)
        if metadata == 1:
            exit(1)
    
    model_inputs = sc.format_input_for_model(metadata)
    
    print(f"Echo Time: {model_inputs[:,0]}")
    print(f"Inversion Time: {model_inputs[:,1]}")
    print(f"Repetition Time: {model_inputs[:,2]}")
    print(f"Diffusion B Value: {model_inputs[:,3]}")
    print(f"SE: {model_inputs[:,4]}")
    print(f"IR: {model_inputs[:,5]}")
    print(f"GR: {model_inputs[:,6]}")
    print(f"EP: {model_inputs[:,7]}")
    print(f"Echo Train Length: {model_inputs[:,8]}")
    print(f"Flip Angle: {model_inputs[:,9]}")

    scan_type, certainty_mat = sc.predict_scan_type(sc.model, model_inputs)
    certainty = certainty_mat[0,scan_type.value]
    certainty_rounded = float("%.3g" %  certainty)
    print(scan_type)
    print(f"Predicted with {certainty_rounded * 100}% certainty")

if __name__ == '__main__':
  fire.Fire(main)
