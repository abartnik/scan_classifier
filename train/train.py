import pandas as pd
from glob import glob
from sklearn.model_selection import train_test_split
from sklearn.metrics import precision_score, accuracy_score, recall_score
from datetime import datetime
from xgboost import XGBClassifier
import re
import json
import begin


scan_type = re.compile(r'(bold)|(dwi)|(MPRAGE)|(SPGR)|(FLAIR)|(TSE_T2w)|(T2star)|(fieldmap)')
scan_sequence = re.compile(r'(SE)|(IR)|(GR)|(EP)')

def join_tuple_string(strings_tuple) -> str:
    return ''.join(strings_tuple)

scan_type_map = {
    "T2star":  0,
    "dti":  1,
    "flair": 2,
    "fmap": 3,
    "bold": 4,
    "fse": 5,
    "mprage": 6,
    "pd": 7,
    "spgr": 8
}

def gather_new_data(input_path):
    train_data = pd.DataFrame(
        columns = [
            'EchoTime', 'InversionTime', 'RepetitionTime', 
            'DiffusionBValue', 'SE', 'IR', 'GR', 'EP', 'EchoTrainLength',
            'FlipAngle', 'scan_type', 'labels'
        ]
    )
    
    for file in glob(f"{input_path}/*json"):
        if '.json' in file:
            res = scan_type.search(file)
            if res:
                row = {i:0 for i in train_data.columns}
                path = file
                label = {'bold':'fmri', 'dwi':'dti', 'T1w':'mprage', 'FLAIR':'flair', 
                        'TSE_T2w':'fse', 'T2star':'T2star', 'fieldmap':'fmap'}
                row['scan_type'] = label.get(res.group())

                with open(path, 'r') as f1:
                    data = json.load(f1)
                    if 'EchoTime' in data.keys():
                        row['EchoTime'] = float(data['EchoTime'] * 1000)
                    if 'InversionTime' in data.keys():
                        row['InversionTime'] = float(data['InversionTime'] * 1000)
                    if 'RepetitionTime' in data.keys():
                        row['RepetitionTime'] = float(data['RepetitionTime'] * 1000)
                    if 'DiffusionBValue' in data.keys():
                        row['DiffusionBValue'] = float(data['DiffusionBValue'])
                    
                    try:
                        seq = list(map(join_tuple_string, scan_sequence.findall(data['ScanningSequence'])))
                    except KeyError:
                        continue
                        
                    row['SE'] = int('SE' in seq)
                    row['IR'] = int('IR' in seq)
                    row['GR'] = int('GR' in seq)
                    row['EP'] = int('EP' in seq)
                    if 'EchoTrainLength' in data.keys():
                        row['EchoTrainLength'] = float(data['EchoTrainLength'])
                    if 'FlipAngle' in data.keys():
                        row['FlipAngle'] = float(data['FlipAngle'])
                    row['labels'] = scan_type_map[row['scan_type']]

                train_data = pd.concat([train_data, pd.DataFrame([row])], ignore_index=True)
    return(train_data)

def format_training_data(new_data):
    X = new_data.drop(labels=['labels','scan_type'], axis=1).to_numpy(dtype='float32')
    y = new_data['labels'].to_numpy(dtype='float32')
    return(train_test_split(X, y, test_size=0.2, random_state=1))

@begin.start
def main(input_path: "Path to the new training data in BIDS format", 
         output_model: "File to save the model to, defaults to model_YYYYMMDD.bin"):
    if output_model is None:
        output_model = f"model_{datetime.today().strftime('%Y%m%d')}.bin"

    new_data = gather_new_data(input_path)
    X_train, X_test, Y_train, Y_test = format_training_data(new_data)

    model = XGBClassifier()
    model.load_model('model.bin')
    model.fit(X_train, Y_train, xgb_model=model.get_booster())

    pred = model.predict(X_test)
    print(f"Accuracy = {accuracy_score(Y_test, pred)}")
    print(f"Micro precision = {precision_score(Y_test, pred, average='micro')}")
    print(f"Macro precision = {precision_score(Y_test, pred, average='macro')}")
    print(f"Micro recall = {recall_score(Y_test, pred, average='micro')}")
    print(f"Macro recall = {recall_score(Y_test, pred, average='macro')}")

    model.save_model(output_model)
