# Training

The model is trained on Echo Time, Inversion Time, Repetition Time, Diffusion B Value, scanning sequence (SE, IR, GR, EP), Echo Train Length, and Flip Angle.
If you have a BIDS dataset with these parameters present in the BIDS sidecar .json file, you can use your data to further train the model to fit your data.
The scan type is derived from the BIDS file name 
(e.g. `acq-MPRAGE` from `sub-01_ses-01_acq-MPRAGE_run-15_T1w.json` will be used to set the label to "MPRAGE" for training)

## Usage

To use the provided script to simplify training, all you need is a pre-sorted BIDS dataset

**N.B.** The model can only be tuned to predict the scan types already present in the model (e.g. MPRAGE, FLAIR, etc.). 
Training the model to recognize new scan types (e.g. SWI, etc.), 
the model would need to be trained from scratch using the original training data.

```
$ python train.py -h

usage: train.py [-h] INPUT_PATH OUTPUT_MODEL

positional arguments:
  INPUT_PATH    Path to the new training data in BIDS format
  OUTPUT_MODEL  File to save the model to, defaults to model_YYYYMMDD.bin

options:
  -h, --help    show this help message and exit
```
