import numpy as np
from scan_classifier_logic import scan_classifier as sc

class Scan:

    def __init__(self, subject_id, exam_id, scan_id, nifti_path, sidecar_path):
        self.subject_id = subject_id
        self.exam_id = exam_id
        self.scan_id = scan_id
        self.nifti_path = nifti_path
        self.sidecar_path = sidecar_path

        self.scan_type: str
        self.z_axis: int
        self.bvec_file: str
        self.bval_file :str

        self.dcm_path: str
        self.nifti_path: str
        self.bids_path: str

        self.certainty_mat: np.ndarray
        self.feature_contributions: dict

if __name__ == "__main__":
    print(help(sc.format_input_for_model))
