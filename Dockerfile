FROM ubuntu:20.04

RUN DEBIAN_FRONTEND="noninteractive" apt-get update && \
    DEBIAN_FRONTEND="noninteractive" apt-get install -y python3 python3-pip cmake dcm2niix pigz

RUN python3 -m pip install wheel
RUN python3 -m pip install setuptools

COPY requirements.txt /opt/scan_classifier/requirements.txt
RUN python3 -m pip install -r /opt/scan_classifier/requirements.txt

RUN mkdir -p /opt/scan_classifier
COPY . /opt/scan_classifier
RUN cd /opt/scan_classifier && python3 setup.py bdist_wheel && python3 setup.py install

RUN chmod +x /opt/scan_classifier/main.py
COPY model.bin /model.bin
ENTRYPOINT [ "/opt/scan_classifier/main.py" ]
