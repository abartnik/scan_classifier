from nipype.interfaces.dcm2nii import Dcm2niix
import nibabel as nb
import pandas as pd
import tempfile
from pathlib import Path
from shutil import copyfile
import begin

from scan_classifier_logic.scan_classifier import (
    model, read_bids_sidecar, format_input_for_model, 
    predict_scan_type, identify_feature_contributions
)

from dcm2bids.Scan import Scan


def get_bids_path(bids_dir, scan_type, scan_id, sub_id, ses_id) -> str:

    if "T1w" in scan_type:
        bids_path = f"{bids_dir}/sub-{sub_id}/ses-{ses_id}/anat/sub-{sub_id}_ses-{ses_id}_run-{scan_id}_T1w"
    if "MPRAGE" in scan_type:
        bids_path = f"{bids_dir}/sub-{sub_id}/ses-{ses_id}/anat/sub-{sub_id}_ses-{ses_id}_acq-MPRAGE_run-{scan_id}_T1w"
    elif "SPGR" in scan_type:
        bids_path = f"{bids_dir}/sub-{sub_id}/ses-{ses_id}/anat/sub-{sub_id}_ses-{ses_id}_acq-SPGR_run-{scan_id}_T1w"
    elif "FLAIR" in scan_type:
        bids_path = f"{bids_dir}/sub-{sub_id}/ses-{ses_id}/anat/sub-{sub_id}_ses-{ses_id}_acq-FLAIR_run-{scan_id}_T2w"
    elif "rsfmri" in scan_type:
        bids_path = f"{bids_dir}/sub-{sub_id}/ses-{ses_id}/func/sub-{sub_id}_ses-{ses_id}_run-{scan_id}_task-rest_bold"
    elif "DWI" in scan_type:
        bids_path = f"{bids_dir}/sub-{sub_id}/ses-{ses_id}/dwi/sub-{sub_id}_ses-{ses_id}_run-{scan_id}_dwi"
    elif "PD" in scan_type:
        bids_path = f"{bids_dir}/sub-{sub_id}/ses-{ses_id}/anat/sub-{sub_id}_ses-{ses_id}_run-{scan_id}_PDw"
    elif "T2star" in scan_type:
        bids_path = f"{bids_dir}/sub-{sub_id}/ses-{ses_id}/anat/sub-{sub_id}_ses-{ses_id}_run-{scan_id}_T2starw"
    elif "FSE" in scan_type:
        bids_path = f"{bids_dir}/sub-{sub_id}/ses-{ses_id}/anat/sub-{sub_id}_ses-{ses_id}_run-{scan_id}_T2w"
    else:
        print(scan_type, scan_id, " No BIDS path")
        bids_path = None
    
    return(bids_path)

@begin.start
def main(raw_mri_dir: "Directory of raw MRI data (DICOM or NIfTI)",
         bids_dir: "Output directory in valid BIDS format",
         dcm: "Specifies that the input data is DICOM. If False, it is assumed the data is NIfTI." = True,
         use_heuristics: "Use heuristics to correct common errors and identify candidate scans" = True,
         dcm2niix_ignore_deriv: "Use the `ignore_deriv` flag for dcm2niix." = False,
         anonymize: "Use either user-provided rules or built-in pattern to map Patient Name/ID to anonymous values" = False,
         anonymizer_rules_file: "User-provided rules for anonymization of Patient Name/IDs in JSON format" = None):

    src_path = Path(raw_mri_dir)
    if not src_path.is_dir():
        print("Please specify a valid directory containing raw MRI data.")
        exit(1)
    
    with tempfile.TemporaryDirectory() as tmp_nii_dir:
        tmp_nii_dir_path = Path(tmp_nii_dir)
        print(tmp_nii_dir_path)

        if dcm == True:
            converter = Dcm2niix()
            converter.inputs.source_dir = src_path
            converter.inputs.output_dir = tmp_nii_dir_path
            converter.inputs.out_filename = "%n!_%i!_%s!_%d"
            converter.inputs.ignore_deriv = dcm2niix_ignore_deriv
            print(converter.cmdline)
            converter.run()
        else:
            tmp_nii_dir_path = src_path

        niftis = list(tmp_nii_dir_path.glob("*nii.gz"))
        scans = []
        for img_file_path in niftis:
            base_name = img_file_path.name.split(".nii.gz")[0]
            sidecar_path = tmp_nii_dir_path / f"{base_name}.json"
            patient_name = img_file_path.name.split("!_")[0]
            patient_id = img_file_path.name.split("!_")[1]
            series_number = img_file_path.name.split("!_")[2]
            series_description = img_file_path.name.split("!_")[3].split(".nii.gz")[0]

            scan = Scan(
                subject_id = patient_name,
                exam_id = patient_id,
                scan_id = series_number,
                nifti_path = img_file_path,
                sidecar_path = sidecar_path
            )
            metadata = read_bids_sidecar(sidecar_path)
            model_inputs = format_input_for_model(metadata)
            scan_type, certainty_mat = predict_scan_type(model, model_inputs)
            scan.scan_type = scan_type.name
            scan.certainty_mat = certainty_mat
            scan.feature_contributions = identify_feature_contributions(model, model_inputs, scan_type.value)
            if use_heuristics ==True:
                if scan.scan_type == "fmap" and 'se' in series_description.lower():
                    scan.scan_type = "T1w"
                if scan.scan_type == "T2star" and 't1' in series_description.lower():
                    scan.scan_type = "T1w"
                if scan.scan_type == "PD" and 't1' in series_description.lower():
                    scan.scan_type = "T1w"

            img = nb.load(str(img_file_path))
            pixdim3 = img.header['pixdim'][3]
            scan.z_axis = pixdim3
            if use_heuristics == True and scan.z_axis >= 6:
                continue

            if 'DWI' in scan.scan_type:
                scan.bvec_file = tmp_nii_dir_path / f"{base_name}.bvec"
                scan.bval_file = tmp_nii_dir_path / f"{base_name}.bval"

            scans.append(scan)

        if anonymize == True:
            def pad_zeroes(i):
                return(f"0{i}" if i < 10 else str(i))
            subject_ids = list(set([ scan.subject_id for scan in scans ]))
            anon_subject_mapping = { subject_id: pad_zeroes(i + 1) for i, subject_id in enumerate(subject_ids) }
            print(anon_subject_mapping)
            anon_rules = {}

        print(f"Creating BIDS dir from {len(scans)} scans...")
        for scan in scans:

            if anonymize == True:
                if not anonymizer_rules_file:
                    if scan.subject_id not in anon_rules.keys():
                            anon_rules[scan.subject_id] = {
                            "anon_subject_id": anon_subject_mapping[scan.subject_id],
                            "exams": []
                        }
                    
                    if len(anon_rules[scan.subject_id]['exams']) == 0:
                        anon_rules[scan.subject_id]['exams'].append({
                            "exam_id": scan.exam_id, 
                            "anon_exam_id": "01"
                        })
                    if scan.exam_id not in [ exam['exam_id'] for exam in anon_rules[scan.subject_id]['exams'] ]:
                        anon_rules[scan.subject_id]['exams'].append({
                            "exam_id": scan.exam_id, 
                            "anon_exam_id": f"0{(len(anon_rules[scan.subject_id]['exams']) + 1)}"
                        })

                else:
                    import json
                    with open (anonymizer_rules_file, "r") as anon_rules_file:
                        anon_rules = json.load(anon_rules_file)

                if scan.subject_id in anon_rules:
                    anon_subject_id = anon_rules[scan.subject_id]['anon_subject_id']

                    for exam in anon_rules[scan.subject_id]['exams']:
                        if scan.exam_id == exam['exam_id']:
                            anon_exam_id = exam['anon_exam_id']
                else:
                    print(f"{scan.subject_id} not found in anonymization rules")
                    print("If you have provided your own anonymizer_rules.json, "\
                          "check that {scan.subject_id} is present and that anonymizer_rules.json is formatted correctly")

                scan.subject_id, scan.exam_id = anon_subject_id, anon_exam_id

            scan.bids_path = get_bids_path(bids_dir = bids_dir, scan_type = scan.scan_type, 
                                           scan_id = scan.scan_id, sub_id = scan.subject_id, 
                                           ses_id = scan.exam_id)

            print(scan.subject_id, scan.exam_id, scan.scan_id, scan.scan_type, scan.z_axis)
            print(scan.nifti_path, scan.bids_path)
            bids_nifti = f"{scan.bids_path}.nii.gz"
            bids_sidecar = f"{scan.bids_path}.json"
            bids_certainty_mat = f"{scan.bids_path}_desc-predcertainty.csv"
            bids_feature_contributions = f"{scan.bids_path}_desc-featurecontributions.csv"

            bids_subject_dir = Path(f"{bids_dir}/sub-{scan.subject_id}")
            bids_session_dir = bids_subject_dir / f"ses-{scan.exam_id}"
            bids_session_dir.mkdir(parents = True, exist_ok = True)

            if "T1w" in scan.scan_type or "MPRAGE" in scan.scan_type or "SPGR" in scan.scan_type or \
                "FLAIR" in scan.scan_type or "PD" in scan.scan_type or "T2star" in scan.scan_type or \
                    "FSE" in scan.scan_type:
                bids_modality_dir = bids_session_dir / "anat"
                bids_modality_dir.mkdir(parents = True, exist_ok = True)
            if "rsfmri" in scan.scan_type:
                bids_modality_dir = bids_session_dir / "func"
                bids_modality_dir.mkdir(parents = True, exist_ok = True)
            if "fmap" in scan.scan_type:
                bids_modality_dir = bids_session_dir / "fmap"
                bids_modality_dir.mkdir(parents = True, exist_ok = True)
            if "DWI" in scan.scan_type:
                bids_modality_dir = bids_session_dir / "dwi"
                bids_modality_dir.mkdir(parents = True, exist_ok = True)
                bids_bvec = f"{scan.bids_path}.bvec"
                bids_bval = f"{scan.bids_path}.bval"
            
            copyfile(scan.nifti_path, bids_nifti)
            copyfile(scan.sidecar_path, bids_sidecar)

            with open(bids_certainty_mat, "w") as certainty_file:
                certainty_file.write("acquisition_type,prediction_certainty\n")
                for index, i in enumerate(scan.certainty_mat[0]):
                    certainty_file.write(f"{index},{i}\n")
            
            feature_contributions_df = pd.DataFrame.from_dict([scan.feature_contributions]).T
            feature_contributions_df.index.name = "feature"
            feature_contributions_df.columns = ["shap_value"]
            feature_contributions_df.to_csv(bids_feature_contributions)

            if "DWI" in scan.scan_type:
                try:
                    copyfile(scan.bvec_file, bids_bvec)
                    copyfile(scan.bval_file, bids_bval)
                except FileNotFoundError:
                    print(f"Could not find bvec/bval for predicted DWI image: {scan.subject_id} - {scan.exam_id} - {scan.scan_id}")
