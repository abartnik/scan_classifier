# XGBoost MRI scan classifier

XGBoost model trained on [ADNI](http://adni.loni.usc.edu/), [PPMI](https://www.ppmi-info.org/), and [OAS3](https://oasis-brains.org/) public MRI datasets to predict scan type of an MRI.

-----------------------------

## Description
This software is intended to be used as a tool for predicting the scan type of an MRI from a DICOM or NIFTI with the appropriate BIDS sidecar. Because the model was trained on data from only a few sources (~10,000 scans total), more heterogeneous training data is needed.

The model requires echo time, repetition time, flip angle, and scanning sequence present in either the DICOM header or BIDS sidecar for input. Inversion time, diffusion b value, and echo train length are imputed if missing, since the model was trained with a value of 0 for any variable that was missing.

### Output
| Label   | Scan type |
|:-------:|:---------:|
| 0 |      T2*w       |
| 1 |       DWI       |
| 2 |    T2w FLAIR    |
| 3 |    Fieldmap     |
| 4 |     rs-fMRI     |
| 5 |   T2w FSE/TSE   |
| 6 |   T1w MPRAGE    |
| 7 | Proton Density  |
| 8 | T1w (IR/F-)SPGR |

## Dependencies
Python:
 - xgboost==1.5.0
 - sklearn==0.23.1
 - pandas==1.0.5
 - numpy==1.19.0
 - pydicom==2.0.0

Chris Rorden's [dcm2niix](https://github.com/rordenlab/dcm2niix) >= v1.0.20210317 for dcm2bids.py

Built using Docker version 20.10.8, build 3967b7d

## Usage
### Installation

It is recommended to build the package in a container using the provided [Dockerfile](Dockerfile) for ease of use and Reproducibility. 

To build: 

`docker build . -t scan_classifier`

To run on a DICOM directory:

`docker run --rm -v /path/to/dicom/directory:/work scan_classifier /work`

To run on a BIDS sidecar (*note that you need to specify the json file as an argument after* `scan_classifier`):

`docker run --rm -v /path/to/BIDS/sidecar.json:/work scan_classifier /work/sidecar.json`

It is also possible to build the package natively/on bare metal to use as a library within Python.

To build:

`python3 setup.py bdist`

`python3 setup.py install`

### Automated DICOM-to-BIDS transformation

The model may be used to automatically transform raw MRI data into a BIDS dataset.
If the source dataset is in NIfTI, the expected file name schema is `<subject_id>!_<exam_id>!_<>!_<scan_id>!_<series description>.nii.gz`.
It is recommended to use this on DICOMs instead of NIfTI where possible.

Usage:

```
python dcm2bids.py -h
usage: dcm2bids.py [-h] [--dcm] [--no-dcm] [--use-heuristics]
                   [--no-use-heuristics] [--dcm2niix-ignore-deriv]
                   [--no-dcm2niix-ignore-deriv] [--anonymize] [--no-anonymize]
                   [--anonymizer-rules-file ANONYMIZER_RULES_FILE]
                   RAW_MRI_DIR BIDS_DIR

positional arguments:
  RAW_MRI_DIR           Directory of raw MRI data (DICOM or NIfTI)
  BIDS_DIR              Output directory in valid BIDS format

optional arguments:
  -h, --help            show this help message and exit
  --dcm
  --no-dcm              Specifies that the input data is DICOM. If False, it
                        is assumed the data is NIfTI. (default: True)
  --use-heuristics
  --no-use-heuristics   Use heuristics to correct common errors and identify
                        candidate scans (default: True)
  --dcm2niix-ignore-deriv
                        Use the `ignore_deriv` flag for dcm2niix. (default:
                        False)
  --no-dcm2niix-ignore-deriv
  --anonymize           Use either user-provided rules or built-in pattern to
                        map Patient Name/ID to anonymous values (default:
                        False)
  --no-anonymize
  --anonymizer-rules-file ANONYMIZER_RULES_FILE, -a ANONYMIZER_RULES_FILE
                        User-provided rules for anonymization of Patient
                        Name/IDs in JSON format (default: None)
```

Example:

```
python dcm2bids.py test/dicoms test/bids --dcm
```

**Automated anonymization:**

`dcm2bids.py` can do some basic anonymization if your DICOMs have sensitive information stored in the 
Patient Name or Patient ID DICOMS tags.
By default, passing the `--anonymize` flag to `dcm2bids.py` will map Patient Name/ID to sequential numbers
if no anonymization rules are provided (i.e. Patient Name A -> sub-1, Patient Name B -> sub-2, etc.).

Users may supply their own anonymization rules in the form of a JSON file following the schema below:

```json
{
  "Patient Name A": {
    "anon_subject_id": "01",
    "exams": [
      {
        "exam_id": "Patient ID A",
        "anon_exam_id": "01"
      },
      {
        "exam_id": "Patient ID B",
        "anon_exam_id": "02"
      }
    ]
  },
  "Patient Name B": {
    "anon_subject_id": "02",
    "exams": [
      {
        "exam_id": "Patient ID C",
        "anon_exam_id": "01"
      }
    ]
  }
}
```

To use your own anonymization rules, 
point `dcm2bids.py` to your anonymization_rules.json file using the `--anonymizer-rules-file` flag:

```
python dcm2bids.py test/dicoms test/bids --dcm --anonymize --anonymizer-rules-file test/anonymizer_rules.json
```

### Predicting scan type within a Python program

An example of full usage can be found in [main.py](./main.py).
Real-world use in a centralized neuroinformatics platform supporting imaging studies
at the University at Buffalo's Center for Biomedical Imaging may be seen
[here](https://gitlab.com/abartnik/cbi-project/-/blob/master/xnat/MriClassification.py?ref_type=heads).

To use as a library inside your Python program, add the following to import:

`from scan_classifier.scan_classifier import *`

**Please note that you will need the model.bin file in your current directory to use as a library.**

#### Read in the scan's metadata

If predicting from DICOM:

`metadata = read_dcm_header('/path/to/DICOM/directory')`

If predicting from BIDS sidecar.json:

`metadata = read_bids_sidecar('/path/to/BIDS/sidebar.json')`

#### Prepare the metadata for the model

`model_inputs = format_input_for_model(metadata)`

#### Predict the scan type using the XGBoost model

`model` is already imported with the package.

`predict_scan_type(model, model_inputs)`

-----------

## Training

Because XGBoost allows for further training of a model by passing `xgb_model=model.get_booster()` to `model.fit()`,
it is possible to tune the model on new data sets. 
We provide a basic script to use additional data from BIDS datasets to further train the model.

See the [README](./train/README.md) in `./train` for more details.

## TODO
 - Add SWI, QSM, ASL, phase/magnitude scan types to model
 - Find more hetergenous data to validate

## License
GPL 3
