1) Convert sessions - dicom to nifti (dcm2niix)
    pass path to session into dcm2niix, keep track of scans as output
    e.g. session\_id\_01 [scan\_id\_1.dcm, scan\_id\_2.dcm, scan\_id\_3.dcm] ->
        session\_id\_01\_niftis [scan\_id\_1.nii.gz, scan\_id\_2.nii.gz, scan\_id\_3.nii.gz]
code 
```
from nipype.interfaces.dcm2nii import Dcm2niix
converter = Dcm2niix()
converter.inputs.source_dir = session.dcm_path
converter.inputs.output_dir = os.path.join('/tmp', subject_id, session_id) # make this directory if it doesn't exist
converter.inputs.out_filename = "%n_%i_%s"
converter.run()
```

2) Map nifti and json filenames to scans in dictionary
```
{
    "subject_id": { [
            "session_1": [
                "scan1": {
                    "scan_type": "type1",
                    "dcm_path": "path_to_dcm",
                    "nifti_path": "path_to_nifti",
                    "json_path": "path_to_json"
                },
                "scan2": {
                    "scan_type": "type2",
                    "path": "path_to_scan"
                }
            ]
        ]
    }
}
```

3) Feed json from dcm2niix into scan classifier model
4) Use nifti pixdim to filter out scans that have < 6mm in the z direction (per GitLab issue #2)
5) Take latest acquisition time among all scans of same type that have requisite number of slices
6) Take the lowest series number of all scans with same acquisition time
7) Map predicted scan type from classifier to BIDS name
```
{
    "subject_id": { [
            "session_1": [
                "scan1": {
                    "scan_type": "type1",
                    "dcm_path": "path_to_dcm",
                    "nifti_path": "path_to_nifti",
                    "json_path": "path_to_json",
                    "bids_path": "subject_id/session_id/scan_type/sub-id_type.<nii.gz/json>"
                }
            ]
        ]
    }
}
```
8) Rename temporary nifti/json files from dcm2niix to bids\_path names
9) Clean up temp directories

BIDS Example:
```
bids
├── dataset_description.json
├── participants.tsv
└── sub-id
    └──session_id
        ├── anat
        ├── dwi
        ├── fmap
        └── func
```
