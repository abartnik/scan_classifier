#!/bin/bash

docker build . -t scan_classifier
docker run --rm -ti scan_classifier /opt/scan_classifier/main.py -h
